## Welcome Hack !!

[Free Software Movement Karnataka, (FSMK)](https://fsmk.org/about/),is organizing 'Welcome Hack' a program which is aimed to build awareness of Free knowledge and Free Software among the general public particularly students. The program intends to address problems around us in an innovative way using tools which uphold the spirit of Free Knowledge. The event will foster the principles of free software to create and spread awareness of the same. 

### Goals

- Create more Free Software evangelists. 
- Develop Hacker Culture using Free Software. 
- Leverage Free Software for the common good. 


### Problems

Problem that Welcome Hack can address could include topics like 
- Application of AI in Education, 
- Free software for Traffic Management, 
- Access to Market Data for Farmers 
- and more.  

### Who can be part of Welcome Hack

Anyone with an urge to learn about Free Software and application can join *Welcome Hack*. 
- Students with a programming or hardware background or who wish to pick up programming in Free Software or apply some cool Open Hardware hacks. 
- Teachers with a technology background who are ready to impart their knowledge and be catalysts for innovation. 
- Product Managers and Data Analysts who can navigate the hackers with the right inputs and help prioritise the right problems. 
- General public with an appetite to learn and apply Free Software Technology. 

We broadly cateogriese humnan resouce involved in conducting Welcome Hack as below

### Organizers
Primary responsibilities include conducting Welcome Hack event monthly. Identifying Welcome Hack theme, preparing content with the support from other volumeter and technology specialist, training volunteers, finding venue and registration process are part of responsibilities of organizing team.

### Volunteers
Who can help the participants during the hackathon or technical sessions. Volunteers will have to undergo special training well in advance before each Welcome Hack program.

### Technical Specialsts
Who can help designing content for Welcome Hack event and training participants. 

### Sponsors
Who can support Welcome Hack  by sponsoring the event monetarily or sponsoring venue, goodies etc. (Add sponosrship deck)

### Upcoming Events

[Welcome Hack 1](https://gitlab.com/fsmk/fsmk-projects/welcome-hack/-/blob/main/1/README.md)

[Welcome Hack 2](https://gitlab.com/fsmk/fsmk-projects/welcome-hack/-/blob/main/2/README.md)

### Completed Events






